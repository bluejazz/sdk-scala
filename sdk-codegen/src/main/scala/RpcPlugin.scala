/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

import io.openkunlun.scalarpc.codegen.*

import java.io.File
import sbt.Keys.*
import sbt.*
import _root_.io.openkunlun.scalarpc.codegen.model.PackageEx
import _root_.io.openkunlun.scalarpc.codegen.model.TraitEx
import _root_.io.openkunlun.scalarpc.codegen.model.MethodEx
import _root_.io.openkunlun.scalarpc.codegen.model.Imported
import _root_.io.openkunlun.scalarpc.codegen.model.NameImport
import _root_.io.openkunlun.scalarpc.codegen.model.RenameImport
import _root_.io.openkunlun.scalarpc.codegen.model.WildcardImport
import _root_.templates.RpcGen.txt.*

import scala.meta.Type.Apply

object RpcPlugin extends AutoPlugin {

  val RpcApp = "app"
  val RpcService = "service"

  object autoImport {

    // Task
    val rpcGen = taskKey[Seq[File]]("Generate rpc models")
    val rpcGenDir = settingKey[File]("rpcGenDir")
    val rpcGenStub = settingKey[Boolean]("rpcGenStub")
    val rpcGenHandler = settingKey[Boolean]("rpcGenHandler")
    val rpcGenExtension = settingKey[Boolean]("rpcGenExtension")
  }

  import autoImport._
  override def trigger = noTrigger

  override val projectSettings: Seq[Setting[_]] = inConfig(Compile)(rpcSettings) ++ inConfig(Test)(rpcSettings)

  def rpcSettings: Seq[Setting[_]] =
    Seq(
      rpcGenDir := sourceManaged.value,
      rpcGenStub := true,
      rpcGenHandler := true,
      sourceGenerators += rpcGenTask.taskValue
    )

  private def rpcGenTask = Def.task {
    rpcGenCode(sourceDirectory.value.**("*.scala").get, rpcGenDir.value, rpcGenStub.value, rpcGenHandler.value)
  }

  private def rpcGenCode(sourceFiles: Seq[File], targetFile: File, genStub: Boolean, genHandler: Boolean): Seq[File] = {
    Parser().files(sourceFiles).flatMap(packageEx => {
      packageEx
        .traits
        .flatMap(traitEx => {
          lookupClass(packageEx, traitEx)
            .map(rpcClass => {
              var rpcFiles = Seq.empty[File]
              if (genStub) {
                val rpcFile = targetFile / s"${rpcClass.packageName}".replace(".", File.separator) / s"${rpcClass.className}Stub.scala"
                IO.write(rpcFile, RpcStub.render(rpcClass).body)
                rpcFiles = rpcFiles :+ rpcFile
              }

              if (genHandler) {
                val rpcFile = targetFile / s"${rpcClass.packageName}".replace(".", File.separator) / s"${rpcClass.className}Handler.scala"
                IO.write(rpcFile, RpcHandler.render(rpcClass).body)
                rpcFiles = rpcFiles :+ rpcFile
              }

              rpcFiles
            })
            .getOrElse(Seq.empty[File])
        })
    })
  }

  private def lookupClass(packageEx: PackageEx, traitEx: TraitEx): Option[RpcClass] = {
    traitEx
      .mods
      .annotations
      .map(it => {
        val imports = packageEx.imports.flatMap(_.imports)
        if (it.syntax.startsWith("@io.openkunlun.scalarpc.RpcService")) {
          val app = it.stringKwargs.getOrElse(RpcApp, "")
          if (null == app || app.isEmpty) {
            None
          } else {
            val service = it.stringKwargs.getOrElse(RpcService, Seq(packageEx.name, traitEx.name).mkString("."))
            Some(RpcClass(lookupImports(imports), packageEx.name, traitEx.name, app, lookMethods(service, imports, traitEx)))
          }

        } else if (it.syntax.startsWith("@RpcService") && imports.exists(_.packageName == "io.openkunlun.scalarpc")) {
          val app = it.stringKwargs.getOrElse(RpcApp, "")
          if (null == app || app.isEmpty) {
            None
          } else {
            val service = it.stringKwargs.getOrElse(RpcService, Seq(packageEx.name, traitEx.name).mkString("."))
            Some(RpcClass(lookupImports(imports), packageEx.name, traitEx.name, app, lookMethods(service, imports, traitEx)))
          }
        } else None
      })
      .collectFirst {
        case Some(r) => r
      }
  }

  private def lookMethods(service: String, imports: Seq[Imported], traitEx: TraitEx): Seq[RpcMethod] = {
    traitEx
      .methods
      .map(it => {
        if (it.parameters.length > 1) {
          None
        } else if (it.returnType.isEmpty) {
          None
        } else if (it.returnType.exists(_.syntax.startsWith("scala.concurrent.Future"))) {
          Some(RpcMethod(Seq(service, it.name).mkString("/"), it.name, lookupParameters(it), it.returnType.map(_.meta.tpe.syntax).getOrElse(""), it.returnType.map(_.args).getOrElse(Seq.empty[String]), it.implementation.nonEmpty))
        } else if (it.returnType.exists(r => r.syntax.startsWith("Future") && imports.exists(_.packageName == "scala.concurrent"))) {
          Some(RpcMethod(Seq(service, it.name).mkString("/"), it.name, lookupParameters(it), it.returnType.map(_.meta.tpe.syntax).getOrElse(""), it.returnType.map(_.args).getOrElse(Seq.empty[String]), it.implementation.nonEmpty))
        } else None
      })
      .collect({
        case Some(r) => r
      })
  }

  private def lookupParameters(methodEx: MethodEx[_]): Seq[RpcParameter] = {
    methodEx
      .parameters
      .headOption
      .map(_.map(it => RpcParameter(it.name, it.`type`.map(_.syntax).getOrElse(""))))
      .getOrElse(Seq.empty)
  }

  private def lookupImports(imports: Seq[Imported]): Seq[String] = {
    imports
      .filterNot(_.packageName.startsWith("io.openkunlun.scalarpc"))
      .collect({
        case NameImport(packageName, typeName)                => s"$packageName.$typeName"
        case RenameImport(packageName, typeName, renamedFrom) => s"$packageName.{$typeName => $renamedFrom}"
        case WildcardImport(packageName)                      => s"$packageName._"
      })
  }
}
