/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scalarpc.codegen.model

import scala.meta._

/**
 * @author kostas.kougios
 *         Date: 10/11/17
 */

case class ModsEx private (meta: ModsEx.Meta) {
  import ModsEx._

  def syntax: String = meta.mods.map(_.syntax).mkString(" ")
  def annotations: Seq[AnnotEx] = meta.annotations
  def isPrivate: Boolean = meta.isPrivate
  def isProtected: Boolean = meta.isProtected
  def isPublic: Boolean = meta.isPublic
  def isOverrides: Boolean = meta.isOverrides
  def isVal: Boolean = meta.isVal
  def isCase: Boolean = meta.isCase

  def withCase: ModsEx = apply(meta.mods :+ mod"case")
  def withPrivate: ModsEx = apply(meta.mods :+ mod"private")
  def withProtected: ModsEx = apply(meta.mods :+ mod"protected")
  def withVal: ModsEx = apply(meta.mods :+ mod"valparam")
  def removeVal(): ModsEx = apply(meta.mods.filterNot {
    case mod"valparam" => true
    case _             => false
  })

  def withFinal: ModsEx = apply(meta.mods :+ mod"final")
  def withOverride: ModsEx = apply(meta.mods :+ mod"override")
  def withImplicit: ModsEx = apply(meta.mods :+ mod"implicit")
}
object ModsEx {
  case class Meta(mods: Seq[Mod]) extends MetaEx with MetaEx.Mods
  def apply(mods: Seq[Mod]): ModsEx = ModsEx(Meta(mods))
  def empty: ModsEx = apply(Nil)
}
