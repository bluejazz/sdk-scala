/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scalarpc.codegen.model

import scala.meta.Tree

/**
 * @author kostas.kougios
 *         Date: 08/09/17
 */
trait PartialParser[T] {
  /**
   * @return a partial function that might parse a Tree
   */
  def parser: PartialFunction[Tree, T]
}
