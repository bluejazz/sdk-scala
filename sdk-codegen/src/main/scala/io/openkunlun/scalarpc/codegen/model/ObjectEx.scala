/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scalarpc.codegen.model

import scala.meta._

/**
 * @author kostas.kougios
 *         08/07/19 - 23:14
 */
case class ObjectEx private (meta: ObjectEx.Meta) extends CodeEx
  with MethodEx.Contains[ObjectEx]
  with MetaEx.Contains
  with MetaEx.ContainsMods[ObjectEx]
  with CodeEx.Name[ObjectEx]
  with ValEx.Contains
  with TemplateEx.Contains[ObjectEx]
  with Extending[ObjectEx] {
  override def tree: Defn.Object = Defn.Object(meta.mods.toList, meta.ename, meta.template)
  override def vals: Seq[ValEx] = template.vals

  override def withMods(mods: ModsEx) = copy(meta = meta.copy(mods = mods.meta.mods))
  override protected def withTemplate(t: Template): ObjectEx = copy(meta = meta.copy(template = t))

  override def name: String = meta.ename.value
  override def withName(name: String): ObjectEx = copy(meta = meta.copy(ename = Term.Name(name)))
}

object ObjectEx extends PartialParser[ObjectEx] {
  case class Meta(mods: Seq[Mod], ename: Term.Name, template: Template) extends MetaEx with MetaEx.Template with MetaEx.Mods

  override def parser: PartialFunction[Tree, ObjectEx] = {
    case q"..$mods object $tname extends { ..$earlydefns } with ..$parents { $self => ..$stats }" =>
      ObjectEx(Meta(mods, tname, Template(earlydefns.toList, parents.toList, self, stats.toList)))
  }
}
