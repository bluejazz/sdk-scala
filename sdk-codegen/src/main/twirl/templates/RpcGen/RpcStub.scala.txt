@(service: io.openkunlun.scalarpc.codegen.RpcClass)
package @service.packageName;
@for(pkgImport <- service.packageImports) {
import @pkgImport
}
import com.google.protobuf.ByteString
import io.openkunlun.scaladsl.client.DaprClient
import io.openkunlun.scaladsl.serialization.DaprObjectSerialization
import io.openkunlun.scaladsl.v1.{InvokeRequest, InvokeService}
import io.openkunlun.scalarpc.RpcErrors
import io.openkunlun.scalarpc.v1.{RpcInvocation, RpcResult}

class @{service.className}Stub(client: DaprClient)(implicit ec: scala.concurrent.ExecutionContext, serialization: DaprObjectSerialization) extends @{service.className} {
    @for(method <- service.methods) {
    override def @{method.methodName}(@{method.parameters.map(it => it.name + ": " + it.tpe).mkString(", ")}): @{method.returnType} = {
      val parameters = Seq(
      @for((param, index) <- method.parameters.zipWithIndex) {
        ByteString.copyFrom(serialization.serialize(@{param.name}))@if(index < method.parameters.length - 1){,}
      }
      )
      val invocation: RpcInvocation = RpcInvocation(parameters)
      val invokeRequest = InvokeRequest(
        "@{method.methodId}",
        Some(com.google.protobuf.any.Any(value = invocation.toByteString))
      )

      for {
        invokeResponse <- client.invokeService(InvokeService("@{service.appName}", Some(invokeRequest)))
        rpcResult = RpcResult.parseFrom(invokeResponse.getData.value.toByteArray)
        _ <- if (rpcResult.cause.nonEmpty) Future.failed(RpcErrors.unpack(rpcResult.getCause.toByteArray)) else Future.unit
      } yield serialization.deserialize@{method.returnTypeArgs.mkString("[", ", ", "]")}(rpcResult.getResult.toByteArray)
    }
    }
}