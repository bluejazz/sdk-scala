/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scalarpc

import scala.annotation.StaticAnnotation

final class RpcService(app: String, service: String = "") extends StaticAnnotation
