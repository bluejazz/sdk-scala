/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.model

final case class StateItem(
  key: String,
  value: AnyRef,
  etag: Option[String] = None,
  metadata: Map[String, String] = Map.empty
//TODO options ???
)
