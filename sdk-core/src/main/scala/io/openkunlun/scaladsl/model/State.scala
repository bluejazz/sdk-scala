/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.model

/**
 * @author ericxin.
 */
final case class State[T](
  value: T,
  key: String,
  etag: Option[String] = None,
  metadata: Map[String, String] = Map.empty
)
