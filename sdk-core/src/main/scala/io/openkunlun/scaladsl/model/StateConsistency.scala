/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.model

object StateConsistency {
  val ConsistencyUnspecified = "CONSISTENCY_UNSPECIFIED"
  val ConsistencyEventual = "CONSISTENCY_EVENTUAL"
  val ConsistencyStrong = "CONSISTENCY_STRONG"
}
