/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.server

/**
 * @author ericxin.
 */
class InvocationHandlerNotFoundException(method: String) extends RuntimeException {
  override def getMessage: String = s"Dapr invocation handler for $method not found."
}
