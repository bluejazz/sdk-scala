/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.server

import com.typesafe.config.Config
import io.openkunlun.scaladsl.util.Strings

import java.util.UUID
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.{ DurationLong, FiniteDuration }
import scala.jdk.CollectionConverters._

/**
 * @author ericxin.
 */
private[server] class DaprAppSettings(config: Config) {

  val daprAppConfig: Config = config.getConfig("io.openkunlun.dapr-app")

  val id: String = UUID.randomUUID().toString.replace("-", "")

  val host: String = daprAppConfig.getString("host")
  val port: Int = daprAppConfig.getInt("port")

  val roles: Seq[String] = daprAppConfig.getStringList("roles").asScala.toSeq.distinct.filter(Strings.nonEmpty)
  val bindings: Seq[String] = daprAppConfig.getStringList("bindings").asScala.toSeq.distinct.filter(Strings.nonEmpty)

  val ttl: Int = daprAppConfig.getDuration("ttl", TimeUnit.SECONDS).toInt
  val weight: Int = daprAppConfig.getInt("weight")
  val warmup: Int = daprAppConfig.getInt("warmup")

  val protocol: String = daprAppConfig.getString("protocol")

  val keepAliveInterval: FiniteDuration = daprAppConfig.getDuration("keep-alive-interval", TimeUnit.SECONDS).seconds
  val connectionIdleInterval: FiniteDuration = daprAppConfig.getDuration("connection-idle-interval", TimeUnit.SECONDS).seconds
  val connectionMaxTimeout: FiniteDuration = daprAppConfig.getDuration("connection-max-timeout", TimeUnit.SECONDS).seconds
}
