/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.client

/**
 * @author ericxin.
 */
class InvocationMethodIsEmptyException extends RuntimeException
