/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.client

/**
 * @author ericxin.
 */
class DaprInvocationResultIsEmptyException(method: String) extends RuntimeException {
  override def getMessage: String = s"Dapr invocation for $method result is empty."
}
