/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.serialization

/**
 * @author ericxin.
 */
trait DaprObjectDeserializer {
  def deserialize[T: Manifest](bytes: Array[Byte]): T
}
