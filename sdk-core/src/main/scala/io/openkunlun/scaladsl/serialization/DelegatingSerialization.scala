/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.serialization

import io.openkunlun.scaladsl.v1.SerializationManifest

trait DelegatingSerialization extends DaprObjectSerialization {

  final override def serialize[T](serializable: T): Array[Byte] = {
    val manifest: SerializationManifest = serializeWithManifest(serializable)
    manifest.toByteArray
  }

  def serializeWithManifest[T](serializable: T): SerializationManifest

  final override def deserialize[T: Manifest](bytes: Array[Byte]): T = {
    val manifest: SerializationManifest = SerializationManifest.parseFrom(bytes)
    deserializeWithManifest(manifest)
  }

  def deserializeWithManifest[T: Manifest](manifest: SerializationManifest): T
}
