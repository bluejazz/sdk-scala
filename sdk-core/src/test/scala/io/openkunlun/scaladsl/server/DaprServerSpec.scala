/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.server

import akka.actor.ActorSystem
import akka.grpc.GrpcClientSettings
import akka.grpc.scaladsl.Metadata
import akka.util.Timeout
import com.google.protobuf.ByteString
import com.google.protobuf.empty.Empty
import com.typesafe.config.ConfigFactory
import io.openkunlun.scaladsl.serialization.SerializationTestProtocol.{ SerializationDto1, SerializationDto2 }
import io.openkunlun.scaladsl.serialization.{ DaprObjectSerialization, JsonSerialization }
import io.openkunlun.scaladsl.v1.{ BindingEventRequest, DaprAppClient, InvokeRequest }

import java.security.cert.CertificateFactory
import scala.concurrent.duration._
import scala.concurrent.{ ExecutionContext, Future }
import scala.io.Source
import scala.util.{ Failure, Success }

/**
 * @author ericxin.
 */
object DaprServerSpec extends App {

  val conf = ConfigFactory.parseString("akka.http.server.preview.enable-http2 = on")
    .withFallback(ConfigFactory.defaultApplication())

  implicit val system = ActorSystem("DaprAppServerSpec", conf)
  implicit val ec = system.dispatcher

  val ser = JsonSerialization(system)

  val server = new DaprServer(system)
  val invocationHandler = new InvocationService[SerializationDto1, SerializationDto2] {
    override val serialization: DaprObjectSerialization = ser
    implicit val deadline: Timeout = 30.seconds

    override def handle(req: InvocationRequest[SerializationDto1], metadata: Metadata)(implicit ec: ExecutionContext, timeout: Timeout): Future[InvocationResponse[SerializationDto2]] = {
      println("AbstractDaprInvocationHandler: " + req.data)
      Future.successful(InvocationResponse(SerializationDto2(key = req.data.key + " - 1", value = req.data.value + " - 1")))
    }

    override val methods: Seq[String] = Seq(SerializationDto1.getClass.toString)
  }
  server.addHandler(invocationHandler)

  val bindingHandler = new BindingService[SerializationDto1, SerializationDto2] {
    override val serialization: DaprObjectSerialization = ser
    implicit val deadline: Timeout = 30.seconds

    override def handle(req: BindingRequest[SerializationDto1], metadata: Metadata)(implicit ec: ExecutionContext, timeout: Timeout): Future[BindingResponse[SerializationDto2]] = {
      println("AbstractDaprBindingHandler: " + req.data)
      Future.successful(BindingResponse(SerializationDto2(key = req.data.key + " - 1", value = req.data.value + " - 1")))
    }

    override val name: String = SerializationDto1.getClass.toString
  }
  server.addHandler(bindingHandler)

  val p12 = Source.fromInputStream(getClass.getClassLoader.getResourceAsStream("certs/server1.key")).mkString
  val certificate = getClass.getClassLoader.getResourceAsStream("certs/server1.pem")

  val fact = CertificateFactory.getInstance("X.509")
  val keyCertChain = fact.generateCertificate(certificate)

  //  server.runHttps("127.0.0.1", 8080, p12, keyCertChain.asInstanceOf[X509Certificate])
  server.start()

  val clientSettings = GrpcClientSettings.fromConfig("dapr.grpc")(system)
  val client = DaprAppClient(clientSettings.withTls(server.isHttps))(system)

  val invoke = InvokeRequest(
    SerializationDto1.getClass.toString,
    Some(com.google.protobuf.any.Any(value = ByteString.copyFrom(ser.serialize(SerializationDto1("key", "value").asInstanceOf[Object])))),
    "application/json"
  )
  client.onInvoke(invoke).onComplete {
    case Success(value) => println("onInvoke result: " + value)
    case Failure(e)     => e.printStackTrace()
  }

  client.listBinding(Empty()).onComplete {
    case Success(value) => println("listBinding result: " + value.bindings)
    case Failure(e)     => e.printStackTrace()
  }

  val bind = BindingEventRequest(
    SerializationDto1.getClass.toString,
    ByteString.copyFrom(ser.serialize(SerializationDto1("key", "value").asInstanceOf[Object])),
    Map("foo" -> "bar")
  )
  client.onBindingEvent(bind).onComplete {
    case Success(value) => println("onBindingEvent result: " + value)
    case Failure(e)     => e.printStackTrace()
  }
}
