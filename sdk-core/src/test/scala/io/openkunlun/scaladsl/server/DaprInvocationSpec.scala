/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.server

import akka.actor.ActorSystem
import akka.grpc.scaladsl.Metadata
import akka.util.Timeout
import com.google.protobuf.ByteString
import io.openkunlun.scaladsl.serialization.SerializationTestProtocol._
import io.openkunlun.scaladsl.serialization.{ AkkaSerialization, DaprObjectSerialization }
import io.openkunlun.scaladsl.server.DaprServerSpec.ser
import io.openkunlun.scaladsl.v1.InvokeRequest

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ ExecutionContext, Future }

/**
 * @author ericxin.
 */
object DaprInvocationSpec extends App {

  val sys = ActorSystem("DaprServerExtensionSpec")

  val server = DaprInvocation(sys)
  val serialization = AkkaSerialization(sys)
  import sys.dispatcher

  val invocationHandler = new InvocationService[SerializationDto1, SerializationDto2] {
    override val serialization: DaprObjectSerialization = ser
    implicit val deadline: Timeout = 30.seconds

    override def handle(req: InvocationRequest[SerializationDto1], metadata: Metadata)(implicit ec: ExecutionContext, timeout: Timeout): Future[InvocationResponse[SerializationDto2]] = {
      println(req.data)
      Future.successful(InvocationResponse(SerializationDto2(key = req.data.key + " - 1", value = req.data.value + " - 1")))
    }

    override val methods: Seq[String] = Seq(SerializationDto1.getClass.toString)
  }
  server.addHandler(invocationHandler)
  val invoke = InvokeRequest(
    "foo",
    Some(com.google.protobuf.any.Any(value = ByteString.copyFrom(serialization.serialize(SerializationDto1("key", "value").asInstanceOf[Object])))),
    "application/json"
  )
  for {
    result1 <- server.onInvoke(invoke, null)
    result2 <- server.onInvoke(InvokeRequest("bar", result1.data, "application/xml"), null)
    result = serialization.deserialize[AnyRef](result2.data.map(_.value.toByteArray).get)
    _ = println(result)
  } yield ()

}
