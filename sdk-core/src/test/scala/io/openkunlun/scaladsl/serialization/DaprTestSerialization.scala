/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.serialization

import io.openkunlun.scaladsl.serialization.SerializationTestProtocol._
import io.openkunlun.scaladsl.v1.SetMetadataRequest

/**
 * @author ericxin.
 */
object DaprTestSerialization {
  def serSerializationDto1(c: SerializationDto1): SetMetadataRequest = SetMetadataRequest(
    c.key, c.value
  )

  def desSerializationDto1(bytes: Array[Byte]): SerializationDto1 = {
    val format = SetMetadataRequest.parseFrom(bytes)
    SerializationDto1(format.key, format.value)
  }

  def serSerializationDto2(c: SerializationDto2): SetMetadataRequest = SetMetadataRequest(
    c.key, c.value
  )

  def desSerializationDto2(bytes: Array[Byte]): SerializationDto2 = {
    val format = SetMetadataRequest.parseFrom(bytes)
    SerializationDto2(format.key, format.value)
  }
}
