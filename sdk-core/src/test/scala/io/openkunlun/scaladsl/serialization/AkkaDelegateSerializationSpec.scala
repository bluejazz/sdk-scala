/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.serialization

import akka.actor.ActorSystem
import com.google.protobuf.ByteString
import io.openkunlun.scaladsl.serialization.SerializationTestProtocol.SerializationDto1
import io.openkunlun.scaladsl.v1.InvokeRequest

/**
 * @author ericxin.
 */
object AkkaDelegateSerializationSpec extends App {

  val sys = ActorSystem("DelegatingSerializationSpec")

  val ser = AkkaDelegateSerialization(sys)

  val bytes = ser.serialize(SerializationDto1("key", "value"))
  val data = com.google.protobuf.any.Any(value = ByteString.copyFrom(bytes))
  val result = ser.deserialize[AnyRef](data.value.toByteArray)
  val result1 = ser.deserialize[AnyRef](bytes)
  println(result)
  println(result1)

  val invoke = InvokeRequest(
    "foo",
    Some(data),
    "application/json"
  )
  val result2 = ser.deserialize[AnyRef](invoke.data.map(_.value.toByteArray).get)
  println(result2)
}
