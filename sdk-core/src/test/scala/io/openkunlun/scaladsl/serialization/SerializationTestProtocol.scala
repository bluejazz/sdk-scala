/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.serialization

/**
 * @author ericxin.
 */
object SerializationTestProtocol {
  trait Format extends Serializable

  final case class SerializationDto1(key: String, value: String) extends Format
  final case class SerializationDto2(key: String, value: String) extends Format

  object Gender extends Enumeration {
    type Type = Value
    val Male = Value("male")
    val Female = Value("female")
  }

  final case class SerializationDto3(key: String, value: String, gender: Gender.Type = Gender.Male) extends Format
  final case class SerializationDto4(key: String, value: String, gender: Gender.Type = Gender.Female) extends Format
}