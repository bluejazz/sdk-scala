/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.serialization

import akka.actor.ExtendedActorSystem
import akka.serialization.Serializer
import io.openkunlun.scaladsl.serialization.SerializationTestProtocol._

import scala.annotation.switch

/**
 * @author ericxin.
 */
class DaprTestSerializer(system: ExtendedActorSystem) extends Serializer {

  import DaprTestSerialization._

  override def identifier: Int = 682582233

  override def includeManifest: Boolean = true

  private val SerializationDto1Class = classOf[SerializationDto1]
  private val SerializationDto2Class = classOf[SerializationDto2]

  @switch
  override def toBinary(o: AnyRef): Array[Byte] = o match {
    case c: SerializationDto1 => serSerializationDto1(c).toByteArray
    case c: SerializationDto2 => serSerializationDto2(c).toByteArray
  }

  @switch
  override def fromBinary(bytes: Array[Byte], manifest: Option[Class[_]]): AnyRef = manifest match {
    case Some(clazz) => clazz match {
      case SerializationDto1Class => desSerializationDto1(bytes)
      case SerializationDto2Class => desSerializationDto2(bytes)
    }
  }
}
