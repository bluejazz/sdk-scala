/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.client

import akka.actor.ActorSystem
import akka.grpc.GrpcClientSettings
import com.google.protobuf.ByteString
import io.openkunlun.scaladsl.serialization.JsonDelegateSerialization
import io.openkunlun.scaladsl.serialization.SerializationTestProtocol.SerializationDto1
import io.openkunlun.scaladsl.v1.{ DaprAppClient, InvokeRequest }

import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.Future

/**
 * @author ericxin.
 */
object DaprAppClientSpec extends App {

  implicit val system = ActorSystem("DaprAppServerSpec")
  implicit val ec = system.dispatcher

  val clientSettings = GrpcClientSettings.fromConfig("dapr.grpc")(system)
  val client = DaprAppClient(clientSettings.withTls(false))(system)

  val serialization = JsonDelegateSerialization(system)

  val invoke = InvokeRequest(
    SerializationDto1.getClass.toString,
    Some(com.google.protobuf.any.Any(value = ByteString.copyFrom(serialization.serialize(SerializationDto1("key", "value").asInstanceOf[Object])))),
    "application/json"
  )

  println(system.settings.config.getConfig("akka.grpc.client.\"dapr.grpc\"").getInt("max-inbound-message-size"))

  val times = (1 to 1).toList

  val t0 = System.currentTimeMillis()

  var success = new AtomicInteger(0)
  var failure = new AtomicInteger(0)

  for {
    _ <- Future.traverse(times) { _ =>
      client.onInvoke(invoke) map { _ => success.incrementAndGet() } recover {
        case e =>
          e.printStackTrace()
          failure.incrementAndGet()
      }
    }
    _ = println("success:" + success)
    _ = println("failure:" + failure)
    _ = println(s"run ${times.length} use time:" + (System.currentTimeMillis() - t0))
  } yield ()
}
