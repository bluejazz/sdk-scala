/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.client

import akka.actor.ActorSystem
import io.openkunlun.scaladsl.serialization.JsonDelegateSerialization
import io.openkunlun.scaladsl.serialization.SerializationTestProtocol.{ SerializationDto1, SerializationDto2 }
import io.openkunlun.scaladsl.server.DaprServerSpec.conf

import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.Future

/**
 * @author ericxin.
 */
object DaprClientSpec extends App {

  implicit val system = ActorSystem("DaprAppServerSpec", conf)
  implicit val ec = system.dispatcher

  val client = DaprClient(system)
  implicit val ser = JsonDelegateSerialization(system)

  val action = InvokeMethodAction(
    "w6s-server",
    SerializationDto1.getClass.toString,
    SerializationDto1("key", "value"),
    Some("application/json")
  )

  println("call remote...")

  val times = (1 to 1).toList

  val t0 = System.currentTimeMillis()

  var success = new AtomicInteger(0)
  var failure = new AtomicInteger(0)

  //  implicit val serialization: DaprObjectSerialization = JsonSerialization(system)

  for {
    _ <- Future.traverse(times) { _ =>
      client.invokeMethod[SerializationDto2](action) map { it =>
        println(it)
        success.incrementAndGet()
      } recover {
        case e =>
          e.printStackTrace()
          failure.incrementAndGet()
      }
    }
    _ = println("success:" + success)
    _ = println("failure:" + failure)
    _ = println(s"run ${times.length} use time:" + (System.currentTimeMillis() - t0))
  } yield ()
}
