/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.client

import akka.actor.ActorSystem
import io.openkunlun.scaladsl.serialization.JsonDelegateSerialization
import io.openkunlun.scaladsl.serialization.SerializationTestProtocol.{ Gender, SerializationDto1, SerializationDto2, SerializationDto3 }
import org.json4s.ext.EnumNameSerializer

import scala.util.{ Failure, Success }

/**
 * @author ericxin.
 */
object DaprActionBuilderSpec extends App {

  implicit val system = ActorSystem("DaprActionBuilderSpec")
  implicit val ec = system.dispatcher

  val ser = JsonDelegateSerialization(system)
  ser.addFormat(new EnumNameSerializer(Gender))

  DaprAction.withJsonDelegateSerialization(system)
    //  DaprAction(system)
    .withHeader("a", "b")
    .withHeaders(Map("c" -> "c"))
    .withTraceId("traceId")
    .invokeMethod("testapp", SerializationDto1.getClass.toString, SerializationDto1("key1", "DaprActionBuilderSpec"))
    .execute[SerializationDto2]
    .onComplete {
      case Success(value) => println(value)
      case Failure(e)     => e.printStackTrace()
    }

  DaprAction(system, JsonDelegateSerialization(system))
    //  DaprAction(system)
    .withHeader("a", "b")
    .withHeaders(Map("c" -> "c"))
    .withTraceId("traceId")
    .invokeMethod("testapp", SerializationDto2.getClass.toString, SerializationDto2("key2", "DaprActionBuilderSpec"))
    .execute[SerializationDto2]
    .onComplete {
      case Success(value) => println(value)
      case Failure(e)     => e.printStackTrace()
    }

  DaprAction.withJsonDelegateSerialization(system)
    //  DaprAction(system)
    .withHeader("a", "b")
    .withHeaders(Map("c" -> "c"))
    .withTraceId("traceId")
    .invokeMethod("testapp", SerializationDto3.getClass.toString, SerializationDto3("key3", "DaprActionBuilderSpec"))
    .execute[SerializationDto2]
    .onComplete {
      case Success(value) => println(value)
      case Failure(e)     => e.printStackTrace()
    }
}
