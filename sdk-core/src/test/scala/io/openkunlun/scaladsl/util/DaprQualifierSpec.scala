/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.util

/**
 * @author ericxin.
 */
object DaprQualifierSpec extends App {
  case class Foo()
  class Bar()

  println(DaprQualifier.build("DaprQualifierSpec", Foo().getClass))
  println(DaprQualifier.build("DaprQualifierSpec", new Bar().getClass))
}
