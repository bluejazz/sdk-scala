/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.context

import akka.actor.ActorSystem

/**
 * @author ericxin.
 */
trait ContextStorage {
  def init(): Unit
  def store[T](ctx: Context)(f: => T): T
  def get(): Context
}

object ContextStorage {

  private val nop = new NopContextStorage()

  def current(system: ActorSystem): ContextStorage = {
    if (system.settings.config.getBoolean("io.openkunlun.context.enable")) {
      system.settings.config.getString("io.openkunlun.context.provider") match {
        case "kamon" => KamonContextStorage(system)
        case _       => nop
      }
    } else nop
  }
}