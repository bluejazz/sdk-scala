/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.context

import akka.actor.{ ExtendedActorSystem, Extension, ExtensionId, ExtensionIdProvider }
import io.openkunlun.scaladsl.context.Context.ContextTag
import play.api.mvc.Request

/**
 * @author ericxin.
 */
object PlayContext extends ExtensionId[PlayContext] with ExtensionIdProvider {
  override def lookup: ExtensionId[PlayContext] = PlayContext
  override def createExtension(system: ExtendedActorSystem) = new PlayContext(system)
}
class PlayContext(system: ExtendedActorSystem) extends Extension {

  private val contextPropagation: ContextStorage = ContextStorage.current(system)

  def init(): Unit = contextPropagation.init()

  def store[T](request: Request[_])(f: => T): T = {
    val tags = request.headers.headers.map(it => it._1 -> it._2)
      .map(it => ContextTag(it._1, it._2))
    contextPropagation.store(new Context(tags))(f)
  }

  def get(): Context = {
    contextPropagation.get()
  }
}
