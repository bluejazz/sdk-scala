/*
 * Copyright 2021-2022 Open Kunlun Technology <https://www.openkunlun.io>
 */

package io.openkunlun.scaladsl.context

/**
 * @author ericxin.
 */
private class NopContextStorage extends ContextStorage {
  override def init(): Unit = ()
  override def store[T](ctx: Context)(f: => T): T = f
  override def get(): Context = Context.empty
}
