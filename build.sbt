import ProjectDependencies._
import ProjectSettings._

ThisBuild / version := "1.0.0-SNAPSHOT"
ThisBuild / organization := "io.openkunlun"

ThisBuild / scalaVersion := "2.13.10"
ThisBuild / scalacOptions ++= Seq("-J-Xss8M")

ThisBuild / isSnapshot := true

lazy val api = (project in file("sdk-api"))
  .settings(name := "sdk-scala-api")
  .settings(commonSettings: _*)
  .dependsOn(core)
  .enablePlugins(HeaderPlugin, AutomateHeaderPlugin)
  .enablePlugins(AkkaGrpcPlugin)

lazy val core = (project in file("sdk-core"))
  .settings(name := "sdk-scala-core")
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= Seq(AkkaActor, AkkaStream, AkkaDiscovery, AkkaPki))
  .settings(libraryDependencies ++= Seq(AkkaHttp, AkkaHttp2Support))
  .settings(libraryDependencies ++= Seq(GrpcCore, GrpcStub, GrpcNettyShaded))
  .settings(libraryDependencies ++= Seq(Json4sJackson, Json4sExt, JacksonDatabind, JacksonDataFormatCbor, jacksonModuleScala))
  .settings(libraryDependencies ++= Seq(ScalapbRuntime))
  .dependsOn(context)
  .enablePlugins(HeaderPlugin, AutomateHeaderPlugin)
  .enablePlugins(AkkaGrpcPlugin)

lazy val rpc = (project in file("sdk-rpc"))
  .settings(name := "sdk-scala-rpc")
  .settings(commonSettings: _*)
  .settings(libraryDependencies += "com.caucho" % "hessian" % "4.0.66")
  .dependsOn(core)
  .enablePlugins(HeaderPlugin, AutomateHeaderPlugin)
  .enablePlugins(AkkaGrpcPlugin)

lazy val context = (project in file("sdk-context"))
  .settings(name := "sdk-scala-context")
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= Seq(KamonCore, KamonScalaFuture, KamonAkka, KamonAkkaHttp, KamonPlay))
  .settings(libraryDependencies ++= Seq(GrpcCore % "provided"))
  .settings(libraryDependencies ++= Seq(AkkaGrpcRuntime % "provided", AkkaDiscovery % "provided", AkkaStream % "provided", AkkaActor % "provided"))
  .settings(libraryDependencies ++= Seq(AkkaHttp % "provided"))
  .settings(libraryDependencies ++= Seq(Play % "provided", AkkaActorTyped % "provided", AkkaSlf4j % "provided", AkkaSerializationJackson % "provided"))
  .enablePlugins(HeaderPlugin, AutomateHeaderPlugin)

lazy val codegen = (project in file("sdk-codegen"))
  .settings(name := "sdk-scala-codegen")
  .settings(scalaVersion := "2.12.16")
  .settings(commonSettings: _*)
  .settings(publishMavenStyle := false)
  .settings(libraryDependencies ++= Seq(Scalameta))
  .settings(libraryDependencies ++= Seq(Javaslang % "test", JunitInterface % "test", Scalatest % "test"))
  .enablePlugins(SbtPlugin, SbtTwirl, HeaderPlugin, AutomateHeaderPlugin)

lazy val root = (project in file("."))
  .aggregate(api, core, rpc, context, codegen)
  .settings(name := "sdk-scala")
  .settings(commonSettings: _*)
  .enablePlugins(HeaderPlugin, AutomateHeaderPlugin)